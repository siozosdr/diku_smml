# README #

This repo contains the assignments took in the Statistical Methods for Machine Learning course in the MSc. in Computer Science program of DIKU.
All the implementations are in Python 2.7.x.

## Assignment 1 ##

Implementation of basic concepts of Machine Learning (Univariate Gaussian distributions, Sampling from a multivariate Gaussian distribution, Means,  Covariance: The geometry of multivariate Gaussian distributions).
Implementation of basic Classification methods (Nearest neighbor, Hyperparameter selection using cross-validation, Data normalization,)

## Assignment 2 ##

Implementation of Classification methods (LDA, LDA and normalization, Bayes optimal classification and probabilistic classification).
Implementation of Regression methods (Maximum likelihood solution, Maximum a posteriori solution, Weighted sum-of-squares )

## Assignment 3 ##

Implementation of Neural Network (NN), Support Vector Machines (SVM).

## Prerequisites: ##
- Python version:  > 2.7.5
- Python libraries: pybrain, operator, csv, math, numpy
- Programs: libSVM-3.20 (SVM implementation), matlab (pca implementation)